====================
OpenDev IRC Meetings
====================

OpenDev maintains an IRC meeting infrastructure.

This repository allows to book a meeting slot and publish it on:

  http://eavesdrop.openstack.org/

Each meeting is described in a YAML file in the meetings/ subdirectory.

Once merged, changes are processed using the yaml2ical module to generate
ical files and HTML files published on https://eavesdrop.openstack.org .

For more information on yaml2ical meeting file format, please see:

  https://opendev.org/opendev/yaml2ical

